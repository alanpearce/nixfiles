{ config
, pkgs
, ...
}: {
  services.xserver.windowManager = {
    i3 = {
      enable = true;
      extraSessionCommands = ''
        ${pkgs.sxhkd}/bin/sxhkd &
      '';
    };
  };
  services.xserver.displayManager.defaultSession = "none+i3";

  environment.systemPackages = with pkgs; [
    i3status
  ];

  imports = [
    ./window-manager.nix
  ];
}
