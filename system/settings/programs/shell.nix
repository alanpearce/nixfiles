{ config
, pkgs
, ...
}: {
  programs.fish = {
    enable = true;
    useBabelfish = true;
  };
  users.users.alan.shell = pkgs.fish;
}
