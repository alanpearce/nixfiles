{ pkgs, ... }: {
  virtualisation.containers = {
    enable = true;
  };
  virtualisation.docker = {
    enable = false;
    storageDriver = "overlay2";
    rootless = {
      enable = true;
      setSocketVariable = true;
      daemon.settings = {
        fixed-cidr-v6 = "fd0c::/80";
        ipv6 = true;
        dns = [
          "1.0.0.1"
          "1.1.1.1"
        ];
      };
    };
  };
  # TODO: autoPrune doesn't affect rootless
  # systemd.timers.docker-prune.timerConfig = {
  #   Persistent = true;
  #   RandomizedDelaySec = 1800;
  # };
}
