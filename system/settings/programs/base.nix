{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    home-manager
    brotli
    lzma
    lzop
    zstd
  ] ++ (lib.optionals (stdenv.isLinux) [
    psmisc
  ]);
}
