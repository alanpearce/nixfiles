{ config
, lib
, pkgs
, ...
}:
{
  services = {
    desktopManager = {
      plasma6.enable = true;
    };
    displayManager = {
      sddm = {
        enable = true;
        enableHidpi = lib.mkDefault false;
      };
    };

    physlock.enable = lib.mkForce false;
  };

  environment.systemPackages = with pkgs; [
    kde-gtk-config
    plasma-browser-integration
  ] ++ (with libsForQt5; [
    merkuro
    kmail
  ]);

  services.picom.enable = false;
  services.blueman.enable = false;
  services.redshift.enable = false;
  programs.partition-manager.enable = true;
}
