{ config
, pkgs
, lib
, ...
}: {
  documentation.info.enable = true;

  environment.systemPackages = with pkgs; [
    epdfview

    lxappearance
    lxrandr
    lxtask

    trash-cli
  ];

  nixpkgs.config.allowUnfree = true;

  services.devmon.enable = true;

  systemd.user.services.trash-clean = {
    path = with pkgs; [ trash-cli ];
    description = "Remove old files from FreeDesktop.org trash";

    serviceConfig = {
      Type = "oneshot";
    };
    script = "trash-empty 30";
  };
  systemd.user.timers.trash-clean = {
    wantedBy = [ "default.target" ];
    timerConfig = {
      OnCalendar = "weekly";
      Persistent = true;
    };
  };

  environment.variables = {
    # This is required so that GTK applications launched from Emacs
    # get properly themed:
    GTK_DATA_PREFIX = "${config.system.path}";
  };

  services.redshift = {
    enable = lib.mkDefault true;
    temperature = {
      day = 6500;
      night = 3600;
    };
  };

  programs.nh = {
    enable = true;
    flake = builtins.toString ../..;
    clean = {
      enable = true;
      extraArgs = "--keep-since 14d";
    };
  };
  nix.gc.automatic = ! config.programs.nh.enable;

  imports = [
    ./services/xserver.nix
  ];
}
