{ config
, pkgs
, ...
}: {
  time.timeZone = "Europe/Berlin";
  location = {
    latitude = 52.586;
    longitude = 13.300;
  };
}
