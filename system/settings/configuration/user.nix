{ config
, pkgs
, ...
}: {
  users.extraUsers.alan = {
    description = "Alan Pearce";
    isNormalUser = true;
    extraGroups = [
      "audio"
      "wheel"
      "lp"
      "adbusers"
      "docker"
      "nitrokey"
      "dialout"
      "pipewire"
      "networkmanager"
      "libvirtd"
      "video"
    ];
    initialPassword = "password";
    home = "/home/alan";
    uid = 1000;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMvcW4Z9VxOQgEJjsRC1uSMwEJ4vru9BwjT+Z50nawp4 alan"
    ];
  };
}
