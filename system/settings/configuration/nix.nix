{ config
, lib
, pkgs
, ...
}: {
  imports = [ ../pin.nix ];
  nix = {
    settings = {
      cores = lib.mkDefault 0;
      use-xdg-base-directories = true;
      keep-outputs = true;
      keep-derivations = true;
      experimental-features = "nix-command flakes";
      warn-dirty = false;
      substituters = [
        "https://nix-community.cachix.org"
        "https://deploy-rs.cachix.org"
        "https://binarycache.alanpearce.eu"
        "https://deploy-rs.cachix.org"
      ];

      trusted-public-keys = [
        "deploy-rs.cachix.org-1:xfNobmiwF/vzvK1gpfediPwpdIP0rpDV2rYqx40zdSI="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "deploy-rs.cachix.org-1:xfNobmiwF/vzvK1gpfediPwpdIP0rpDV2rYqx40zdSI="
        "binarycache.alanpearce.eu:ZwqO3XMuajPictjwih8OY2+RXnOKpjZEZFHJjGSxAI4="
      ];
    };

    gc = {
      automatic = lib.mkDefault true;
      options = lib.mkDefault "--delete-older-than 14d";
    };
  };
}
