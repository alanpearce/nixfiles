{ config
, lib
, pkgs
, ...
}: {
  imports = [
    ./nix.nix
  ];

  nix = {
    settings = {
      auto-optimise-store = true;
    };
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
  };

  nixpkgs.config.allowUnfree = true;

  system.autoUpgrade = {
    enable = true;
    flags = [ "--max-jobs" "2" ];
  };
  systemd.services.nixos-upgrade = {
    script = pkgs.lib.mkForce ''
      ${pkgs.nix}/bin/nix-channel --update
      ${config.system.build.nixos-rebuild}/bin/nixos-rebuild boot --no-build-output ${toString config.system.autoUpgrade.flags}
    '';
  };
}
