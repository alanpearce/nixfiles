{ config
, pkgs
, lib
, ...
}: {
  imports = [
    ./configuration/nix.nix
  ];
  services.nix-daemon = {
    enable = true;
    enableSocketListener = true;
  };

  environment.profiles = with lib; mkMerge [
    (mkOrder 800 [ "\${XDG_STATE_HOME:-$HOME/.local/state}/nix/profile" ])
    [ "/run/current-system/sw" "/nix/var/nix/profiles/default" ]
  ];

  environment.darwinConfig = "$HOME/.config/nixpkgs/darwin-configuration.nix";
  nix = {
    daemonIOLowPriority = true;
    gc = {
      user = "alan";
      options = "--max-freed $((25 * 1024**3 - 1024 * $(df -P -k /nix/store | tail -n 1 | awk '{ print $4 }')))";
    };
    settings.extra-platforms = "aarch64-darwin x86_64-darwin";

    settings.trusted-users = [ "@admin" ];
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  # needed so that nix-darwin can activate the system as root
  security.sudo.extraConfig = ''
    Defaults	env_keep += "NIX_PATH"
  '';

  services.lorri.enable = true;
  launchd.user.agents.lorri = {
    serviceConfig = {
      RunAtLoad = lib.mkForce false;
      Sockets = {
        daemon = {
          SockPathName = "${(builtins.getEnv "HOME")}/Library/Caches/com.github.target.lorri.lorri.lorri/daemon.socket";
        };
      };
    };
  };

  environment.launchDaemons = {
    "limit.maxfiles.plist" = {
      text = ''
        <?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
                "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
        <plist version="1.0">
          <dict>
            <key>Label</key>
            <string>limit.maxfiles</string>
            <key>ProgramArguments</key>
            <array>
              <string>launchctl</string>
              <string>limit</string>
              <string>maxfiles</string>
              <string>64000</string>
              <string>524288</string>
            </array>
            <key>RunAtLoad</key>
            <true/>
          </dict>
        </plist>
      '';
    };
  };

  environment.systemPackages = with pkgs; [
    watch
    coreutils-prefixed
    nixos-rebuild # for rebuilds of remote systems
  ];
}
