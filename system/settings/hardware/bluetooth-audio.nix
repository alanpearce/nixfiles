{ config
, pkgs
, ...
}: {
  hardware = {
    bluetooth = {
      package = pkgs.bluezFull;
    };
    pulseaudio = {
      extraModules = with pkgs; [
        pulseaudio-modules-bt
      ];
    };
  };
}
