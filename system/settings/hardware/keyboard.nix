{ config
, pkgs
, ...
}: {
  services.xserver.xkb = {
    layout = "us,de";
    variant = "intl-unicode,nodeadkeys";
    options = "altwin:prtsc_rwin,caps:escape_shifted_capslock";
  };
}
