{ config
, pkgs
, ...
}: {
  networking.wireless.iwd = {
    enable = true;
  };
  users.users.alan.extraGroups = [ "network" ];

  services.connman.wifi.backend = "iwd";
}
