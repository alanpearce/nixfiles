{ config
, pkgs
, lib
, ...
}:
{
  services.xserver.videoDrivers = [ "nvidia" ];
  boot.initrd.kernelModules = [ "nvidia" ];
  boot.extraModulePackages = [ config.boot.kernelPackages.nvidia_x11 ];

  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement = {
      enable = true;
    };
    package = config.boot.kernelPackages.nvidiaPackages.stable;
  };

  nixpkgs.config.allowUnfree = true;
  services.picom = {
    enable = lib.mkDefault true;
  };

  services.displayManager.sddm.wayland.enable = false;
  services.displayManager.defaultSession = "plasmax11";
}
