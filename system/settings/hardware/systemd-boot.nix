{ config
, pkgs
, ...
}: {
  boot.loader.systemd-boot = {
    enable = true;
    editor = false; # Don't allow modification
  };
  boot.loader.efi.canTouchEfiVariables = false;
  console.earlySetup = true;
}
