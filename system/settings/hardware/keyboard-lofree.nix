{ ... }: {
  systemd.tmpfiles.settings."fix-lofree-keyboard" = {
    "/sys/module/hid_apple/parameters/fnmode" = {
      w = {
        argument = "2";
      };
    };
  };
}
