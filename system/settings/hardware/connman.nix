{ config
, pkgs
, ...
}: {
  services.connman = {
    enable = true;
    enableVPN = false;
    extraFlags = [ "--nodnsproxy" ];
  };
  networking.useDHCP = false;

  environment.systemPackages = with pkgs; [
    connman-gtk
    connman-notify
    connman_dmenu
  ];
}
