{ config
, pkgs
, ...
}: {
  services.pipewire = {
    enable = true;
    audio.enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };

  sound.enable = true;

  environment.systemPackages = with pkgs; [
    pamixer
  ];
}
