{ config
, pkgs
, lib
, ...
}: {
  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = false;
    };
  };
  systemd.services.bluetooth.restartIfChanged = false;

  environment.systemPackages = with pkgs; [
    bluez-tools
  ];

  services.blueman = {
    enable = lib.mkDefault true;
  };
}
