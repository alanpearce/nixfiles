{ config
, pkgs
, ...
}: {
  environment.systemPackages = with pkgs; [
    exfat
    efibootmgr
    dmidecode
    hdparm
    pciutils
    usbutils
  ];

  services.logind = {
    extraConfig = ''
      IdleAction=suspend
      IdleActionSec=1800
      HandlePowerKey=suspend
      HandlePowerKeyLongPress=poweroff
      PowerKeyIgnoreInhibited=yes
    '';
  };
  services.udev.extraRules = ''
    # set scheduler for NVMe
    ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="kyber"
    ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/iosched/read_lat_nsec}="100000"
    # set scheduler for SSD and eMMC
    ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
    # set scheduler for rotating disks
    ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="kyber"
  '';

  boot.kernelModules = [ "tcp_bbr" ];
  boot.kernel.sysctl = {
    "net.core.default_qdisc" = "cake";
    "net.ipv4.tcp_allowed_congestion_control" = "bbr illinois reno lp";
    "net.ipv4.tcp_congestion_control" = "bbr";
    "net.ipv4.tcp_fastopen" = 3;
    "net.ipv4.tcp_slow_start_after_idle" = 0;
    "net.ipv4.tcp_mtu_probing" = 1;
    "net.core.rmem_max" = 2500000;
    "net.core.wmem_max" = 2500000;
  };

  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };
  boot.tmp.useTmpfs = true;

  fileSystems."/".options = [ "noatime" "nodiratime" ];
  fileSystems."/home".options = [ "noatime" "nodiratime" ];
}
