{ config
, pkgs
, lib
, ...
}: {
  boot.loader = {
    grub = {
      enable = true;
      splashImage = null;
      version = 2;
      device = "nodev";
      efiSupport = true;
      useOSProber = lib.mkDefault true;
    };
    efi.canTouchEfiVariables = true;
  };
}
