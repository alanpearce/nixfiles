{ config
, pkgs
, lib
, ...
}: {
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    ipv6 = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      userServices = true;
      workstation = true;
    };
  };
}
