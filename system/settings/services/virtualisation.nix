{ config
, pkgs
, ...
}: {
  virtualisation.libvirtd = {
    enable = true;
    onBoot = "ignore";
    onShutdown = "shutdown";
    qemu = {
      package = pkgs.qemu_kvm;
      ovmf.enable = true;
      runAsRoot = false;
    };
  };
  programs.virt-manager = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    OVMF
  ];
}
