{ config
, pkgs
, ...
}:
let
  workgroup = "WORKGROUP";
in
{
  services.samba-wsdd = {
    inherit workgroup;
    enable = true;
  };
  networking.firewall.allowedTCPPorts = [ 5357 ];
  networking.firewall.allowedUDPPorts = [ 3702 ];

  services.samba = {
    enable = true;
    enableNmbd = false;
    package = pkgs.samba.override { enableMDNS = true; };
    openFirewall = true;
    securityType = "user";
    extraConfig = ''
      workgroup = ${workgroup}
      mdns name = mdns
      min protocol = smb2
      security = user
      #use sendfile = yes
      guest account = nobody
      map to guest = bad user
    '';
  };
}
