{ config
, lib
, pkgs
, ...
}:
with lib; {
  services.xserver = {
    enable = true;
    exportConfiguration = true;
  };

  console.useXkbConfig = true;

  xdg.sounds.enable = false;

  environment.systemPackages = with pkgs; [
    xorg.xmodmap
    xorg.xinit
    xorg.xev
    xorg.xdpyinfo
    xclip
    xfontsel

    arc-theme
    arc-icon-theme

    gtk-engine-murrine
    gtk_engines
  ];

  fonts = {
    fontDir.enable = true;
    enableDefaultPackages = false;
    fontconfig = {
      antialias = true;
      useEmbeddedBitmaps = true;
      defaultFonts = {
        monospace = [ "Office Code Pro" ];
        sansSerif = [ "Source Sans Pro" ];
        serif = [ "Source Serif Pro" ];
      };

      localConf = ''
        <?xml version='1.0'?>
        <!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
        <fontconfig>
          <match target="font">
            <test name="weight" compare="more">
              <const>medium</const>
            </test>
            <edit name="autohint" mode="assign">
              <bool>false</bool>
            </edit>
          </match>
        </fontconfig>
      '';
    };
    packages = with pkgs;
      [
        gohufont
        dina-font
        terminus_font

        corefonts

        xorg.fontmiscmisc
        xorg.fontcursormisc
      ]
      ++ lib.optionals config.fonts.fontconfig.antialias [
        cantarell-fonts

        fira
        fira-code
        fira-mono
        ibm-plex

        oxygenfonts
        noto-fonts-color-emoji

        office-code-pro
        source-code-pro
        source-sans-pro
        source-serif-pro
      ];
  };
}
