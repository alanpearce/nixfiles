{ config
, pkgs
, ...
}: {
  services.pipewire.systemWide = false;
  services.shairport-sync = {
    enable = true;
    group = "pipewire";
  };
  systemd.services.shairport-sync.serviceConfig.Environment = "PULSE_SERVER=tcp:127.0.0.1:4713";
}
