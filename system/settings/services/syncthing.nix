{ config
, pkgs
, ...
}: {
  services.syncthing = {
    enable = true;
    user = "alan";
    group = "users";
    openDefaultPorts = true;
    systemService = false;
    dataDir = "/home/alan/.config/syncthing";
  };
  systemd.user.services.syncthing.environment = {
    LOGGER_DISCARD = "1";
  };
}
