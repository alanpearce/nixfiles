{ config
, pkgs
, lib
, ...
}:
{
  boot.loader.timeout = lib.mkDefault 1;
  services.irqbalance.enable = true;
}
