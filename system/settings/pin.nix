{ pkgs, ... }:
let
  lib = import ../../lib { inherit pkgs; };
  sources = import ../../nix/sources.nix;
in
{
  nix.nixPath = lib.mkNixPath sources;
  nix.registry.nixpkgs.to = {
    type = "path";
    path = sources.nixpkgs;
  };
}
