{ config
, pkgs
, ...
}: {
  security.rtkit.enable = true;
  programs.steam.enable = true;
  programs.gamemode = {
    enable = true;
    settings = {
      general = {
        renice = 5;
      };

      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
      };
    };
  };
  fonts.fontconfig.cache32Bit = true;
  hardware.steam-hardware.enable = true;
  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };
  hardware.pulseaudio.support32Bit = true;
  services.pipewire.alsa.support32Bit = true;

  systemd = {
    extraConfig = ''
      DefaultLimitNOFILE=524288
    '';
    user.extraConfig = ''
      DefaultLimitNOFILE=524288
    '';
  };

  environment.systemPackages = with pkgs; [
    ludusavi
  ];

  services.ananicy = {
    enable = true;
  };

  networking.firewall = {
    allowedUDPPorts = [
      27031
      27036
    ];
    allowedTCPPorts = [
      27036
      27037
    ];
  };
}
