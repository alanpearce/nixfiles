{ ... }: {
  imports = [
    ./settings/darwin.nix
    ./settings/programs/base.nix
    ./settings/programs/shell.nix
  ];

  networking = {
    hostName = "mba";
  };

  services.activate-system.enable = true;

  environment.variables.LANG = "en_GB.UTF-8";

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;

  # You should generally set this to the total number of logical cores in your system.
  # $ sysctl -n hw.ncpu
  nix.settings = {
    max-jobs = 8;
    cores = 4;
    auto-optimise-store = false; # https://github.com/NixOS/nix/issues/7273
  };

  nix.buildMachines = [
    {
      protocol = "ssh-ng";
      sshUser = "nixremote";
      hostName = "linde.alanpearce.eu";
      system = "aarch64-linux";
      maxJobs = 2;
      speedFactor = 1;
      supportedFeatures = [ ];
    }
  ];

  nix.linux-builder = {
    maxJobs = 4;
    config = { pkgs, ... }: {
      virtualisation = {
        darwin-builder = {
          diskSize = 60 * 1024;
          memorySize = 8 * 1024;
        };
        cores = 4;
      };
      # don't go crazy with this setup, it rebuilds the VM
      imports = [
        ./settings/configuration/user.nix
        ./settings/programs/shell.nix
      ];
      environment.systemPackages = with pkgs; [
        kitty.terminfo
        hello
      ];
    };
    systems = [ "aarch64-linux" ];
  };
}
