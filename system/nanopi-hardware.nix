# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/6adfb32b-ebe9-4116-86e8-829d2c9dc79d";
      fsType = "ext4";
    };

  fileSystems."/mnt/sd" =
    { device = "/dev/disk/by-uuid/79d9c190-1728-42ae-8cfd-b03d4a10bdb3";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/mnt/sd/boot";
      fsType = "none";
      options = [ "bind" ];
    };

  swapDevices = [ ];

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
}
