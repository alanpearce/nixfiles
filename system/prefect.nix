{ config
, pkgs
, ...
}: {
  imports = [
    ./settings/base.nix
    ./settings/configuration/berlin.nix
    ./settings/configuration/british-english.nix
    ./settings/configuration/nix-linux.nix
    ./settings/configuration/user.nix
    ./settings/hardware/audio.nix
    ./settings/hardware/bare-metal.nix
    ./settings/hardware/mouse.nix
    ./settings/hardware/systemd-boot.nix
    ./settings/hardware/nvidia-gpu.nix
    ./settings/hardware/keyboard.nix
    ./settings/hardware/keyboard-lofree.nix
    ./settings/services/syncthing.nix
    ./settings/services/virtualisation.nix
    ./settings/services/zeroconf.nix
    ./settings/user-interface.nix
    ./settings/programs/base.nix
    ./settings/programs/gnupg.nix
    ./settings/programs/kde.nix
    ./settings/programs/shell.nix
    ./settings/programs/docker.nix
    ./settings/gaming.nix
    <nixos-hardware/common/cpu/amd>
    <nixos-hardware/common/cpu/amd/pstate.nix>
    <nixos-hardware/common/pc/ssd>
    <nixos-hardware/common/pc>
    <nixos-hardware/common/gpu/nvidia>
  ];

  nixpkgs.hostPlatform = "x86_64-linux";

  services.xserver.screenSection = ''
    Option "MetaModes" "2560x1440_165 +0+0 { }"
    #Option "MetaModes" "2560x1440_120 +0+0 { AllowGSYNC=Off }"
  '';
  services.xserver.monitorSection = ''
    Option "DPMS"
  '';
  services.displayManager.autoLogin = {
    user = "alan";
    enable = true;
  };

  boot.kernelPackages = pkgs.linuxPackages_xanmod;
  boot.extraModulePackages = with config.boot.kernelPackages; [
    zenpower
  ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];

  boot.initrd.supportedFilesystems = [ "xfs" "ext4" ];
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-partlabel/nixos";
      fsType = "xfs";
      options = [ "lazytime" "nodiscard" ];
    };

    "/boot" = {
      device = "/dev/disk/by-partlabel/efi-boot";
      fsType = "vfat";
    };

    "/home" = {
      device = "/dev/disk/by-partlabel/home";
      fsType = "ext4";
      options = [ "lazytime" "nodiscard" ];
    };

    "/mnt/data" = {
      device = "/dev/disk/by-partlabel/data";
      fsType = "ntfs3";
      options = [ "uid=1000" "gid=100" "x-systemd-automount" "nofail" ];
    };
  };

  swapDevices = [
    {
      label = "swap";
      discardPolicy = "once";
    }
  ];

  services.xserver = {
    xautolock.enable = pkgs.lib.mkForce false;
  };

  services.openssh = {
    enable = true;
    openFirewall = false;
    startWhenNeeded = true;
  };
  systemd.services."ssh-no-sleep@" = {
    bindsTo = [ "sshd@%i.service" ];
    wantedBy = [ "sshd@.service" ];
    serviceConfig.ExecStart = "${pkgs.systemd}/bin/systemd-inhibit --mode block --what sleep --who 'ssh session '%I --why 'session still active' ${pkgs.coreutils}/bin/sleep infinity";
  };

  systemd.network = {
    enable = true;
    networks."40-enp7s0" = {
      dhcpV4Config = {
        UseDNS = true;
      };
      ipv6AcceptRAConfig = {
        UseDNS = true;
      };
    };
  };
  networking = {
    useDHCP = false;
    useNetworkd = true;
    interfaces.enp7s0 = {
      useDHCP = true;
    };
  };
  networking.nftables = {
    enable = true;
  };
  networking.firewall = {
    allowedTCPPorts = [ 80 443 139 445 1024 ];
    extraInputRules = ''
      ip saddr 10.0.0.0/8 accept
      ip6 saddr { fd00::/8, fe80::/10 } accept
    '';
  };

  networking = {
    hostName = "prefect";
  };

  system.stateVersion = "23.05";

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  nix.settings.trusted-users = [ "root" "nixremote" ];
  services.displayManager.hiddenUsers = [ "nixremote" ];
  users.users.nixremote = {
    shell = "/bin/sh";
    isNormalUser = true;
    home = "/var/lib/nixremote";
    createHome = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBxa7lxDu0M4chats/VvpFzjT3ruexKa3J9UC6ASo3bN root@NanoPi.lan"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE9of82WBHK8nr8L9RGeieLMfcAWaFCeCkmvYHM9LCuT nanopi"
    ];
  };
}
