{ pkgs }:
with builtins;
let
  inherit (pkgs) lib;
in
rec {
  importPathStore = p: "${append ../. "/${p}"}";

  mkPathable = s: toString (
    if s.type == "local"
    then (lib.path.append ../. s.outPath)
    else s.outPath
  );

  mkNixPath = sources: lib.attrsets.mapAttrsToList
    (k: v: "${k}=${mkPathable v}")
    (removeAttrs sources [ "__functor" ]);
}
