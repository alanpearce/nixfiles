{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-small.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    nix-index-database.url = "github:Mic92/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";
    darwin.url = "github:lnl7/nix-darwin/master";
    darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    emacs-overlay.inputs.nixpkgs.follows = "nixpkgs";
    secrets = {
      flake = false;
    };
    utils.url = "github:numtide/flake-utils";
    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    deploy-rs.url = "github:serokell/deploy-rs";
    searchix.url = "git+https://git.alanpearce.eu/searchix";
  };

  outputs =
    inputs@
    { self
    , utils
    , nixpkgs
    , nixpkgs-small
    , nixos-hardware
    , home-manager
    , darwin
    , nix-index-database
    , secrets
    , emacs-overlay
    , agenix
    , deploy-rs
    , searchix
    , ...
    }:
    let
      readOverlays = path:
        let content = builtins.readDir path; in
        map (n: import (path + ("/" + n)))
          (builtins.filter
            (n:
              (builtins.match ".*\\.nix" n != null &&
              # ignore Emacs lock files (.#foo.nix)
              builtins.match "\\.#.*" n == null) ||
              builtins.pathExists (path + ("/" + n + "/default.nix")))
            (builtins.attrNames content));

      mkHomeConfiguration = { modules, system }: home-manager.lib.homeManagerConfiguration {
        pkgs = import nixpkgs {
          inherit system;
          overlays = readOverlays (toString ./overlays) ++ [ emacs-overlay.overlay ];
        };

        inherit modules;
        extraSpecialArgs = {
          inherit inputs system;
        };
      };
    in
    {
      nixosConfigurations.prefect = nixpkgs.lib.nixosSystem {
        system = utils.lib.system.x86_64-linux;
        specialArgs = { inherit inputs; };
        modules = [
          ./system/prefect.nix
        ] ++ (with nixos-hardware.nixosModules; [
          common-cpu-amd
          common-cpu-amd-pstate
          common-pc-ssd
          common-pc
          common-gpu-nvidia-nonprime
        ]);
      };
      nixosConfigurations.nanopi = nixpkgs-small.lib.nixosSystem {
        system = utils.lib.system.aarch64-linux;
        specialArgs = { inherit inputs; };
        modules = [
          agenix.nixosModules.default
          ./system/nanopi.nix
        ];
      };
      nixosConfigurations.linde = nixpkgs-small.lib.nixosSystem {
        system = utils.lib.system.aarch64-linux;
        specialArgs = { inherit inputs; };
        modules = [
          agenix.nixosModules.default
          searchix.nixosModules.web
          ./system/linde.nix
        ];
      };
      darwinConfigurations.mba = darwin.lib.darwinSystem {
        system = utils.lib.system.aarch64-darwin;
        specialArgs = { inherit inputs; };
        modules = [ ./system/mba.nix ];
      };
      homeConfigurations."alan@mba" = mkHomeConfiguration {
        system = utils.lib.system.aarch64-darwin;
        modules = [
          ./user/mba.nix
          nix-index-database.hmModules.nix-index
          (secrets + "/default.nix")
          (secrets + "/ssh.nix")
        ];
      };
      homeConfigurations."alan@prefect" = mkHomeConfiguration {
        system = utils.lib.system.x86_64-linux;
        modules = [
          ./user/prefect.nix
          nix-index-database.hmModules.nix-index
          (secrets + "/default.nix")
          (secrets + "/ssh.nix")
        ];
      };
      homeConfigurations."alan@nanopi" = mkHomeConfiguration {
        system = utils.lib.system.aarch64-linux;
        modules = [
          ./user/nanopi.nix
          nix-index-database.hmModules.nix-index
          (secrets + "/default.nix")
        ];
      };
      homeConfigurations."alan@linde" = mkHomeConfiguration {
        system = utils.lib.system.aarch64-linux;
        modules = [
          ./user/server.nix
          nix-index-database.hmModules.nix-index
          (secrets + "/default.nix")
        ];
      };

      checks = builtins.mapAttrs
        (system: deployLib:
          deployLib.deployChecks self.deploy)
        deploy-rs.lib;

      deploy = {
        remoteBuild = true;
        nodes.linde = {
          hostname = "linde";
          profiles.system = {
            user = "root";
            interactiveSudo = true;
            path = deploy-rs.lib.${utils.lib.system.aarch64-linux}.activate.nixos
              self.nixosConfigurations.linde;
          };
          profiles.alan = {
            user = "alan";
            path = deploy-rs.lib.${utils.lib.system.aarch64-linux}.activate.home-manager
              self.homeConfigurations."alan@linde";
          };
        };
        nodes.nanopi = {
          hostname = "nanopi";
          profiles.system = {
            user = "root";
            interactiveSudo = true;
            path = deploy-rs.lib.${utils.lib.system.aarch64-linux}.activate.nixos
              self.nixosConfigurations.nanopi;
          };
          profiles.alan = {
            user = "alan";
            path = deploy-rs.lib.${utils.lib.system.aarch64-linux}.activate.home-manager
              self.homeConfigurations."alan@nanopi";
          };
        };
      };
    } // utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
    in
    {
      devShells = {
        default = pkgs.mkShell {
          packages = [
            deploy-rs.packages.${system}.default
            agenix.packages.${system}.default
          ];
        };
      };
    });
}
