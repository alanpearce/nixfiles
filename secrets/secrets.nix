let
  users = {
    alan = [
      "age1se1qdx3wrvaxevk3g40ngqreqc9n4gl0rwcjdvnptz5vw96jjjuf2rv2wp8c5m" # mba age-plugin-se
    ];
  };

  machines = {
    linde = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHHdh3J7dEmh9G+CVmzFEC8/ont35ZXpCFcpLUO863vC";
    nanopi = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG/KOwhb4pyuw4U8hnkPAbRNk6o41Fmvsa67cY6MHA9k";
  };

  secrets = with machines; {
    acme = [ linde nanopi ];

    binarycache = [ linde ];
    paperless = [ linde ];
    powerdns = [ linde ];
    dex = [ linde ];

    dyndns = [ nanopi ];
    syncthing = [ nanopi ];
  };
in
builtins.listToAttrs (
  map
    (secretName: {
      name = "${secretName}.age";
      value.publicKeys = secrets.${secretName} ++ users.alan;
    })
    (builtins.attrNames secrets)
)
