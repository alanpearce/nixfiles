self: super: {
  firefox-bin-unwrapped = super.firefox-bin-unwrapped.override { systemLocale = "en-GB"; };
  firefox-devedition-bin-unwrapped = super.firefox-devedition-bin-unwrapped.override { systemLocale = "en-GB"; };
}
