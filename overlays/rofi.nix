self: super: {
  rofi = super.rofi.overrideAttrs (oldAttrs: rec {
    postInstall = ''
      ln $out/bin/rofi $out/bin/dmenu
    '';
  });
}
