self: super: {
  vimPlugins = super.vimPlugins.extend (final: prev: {
    coc-tabnine =
      let
        inherit (super) tabnine;
      in
      prev.coc-tabnine.overrideAttrs {
        buildInputs = [ tabnine ];

        postFixup = ''
          mkdir -p $target/binaries/${tabnine.version}
          ln -s ${tabnine}/bin/ $target/binaries/${tabnine.version}/${tabnine.passthru.platform}
        '';
      };
  });
}
