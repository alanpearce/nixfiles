self: super:
let
  personal = import <personal> {
    pkgs = self;
  };
in
{
  inherit personal;
  enchant = personal.enchant-configurable.override {
    withHspell = false;
    withAspell = false;
  };
}
