{ pkgs, ... }: {
  imports = [
    ./settings/base.nix
    ./settings/development/base.nix
    ./settings/development/javascript.nix
    ./settings/development/golang.nix
    ./settings/development/lisp.nix
    ./settings/development/web.nix
    ./settings/darwin.nix
    ./settings/emacs.nix
    ./settings/fish.nix
    ./settings/git.nix
    ./settings/kitty.nix
    ./settings/nixpkgs.nix
    ./settings/ssh.nix
    ./settings/tabnine.nix
    ./settings/user-interface.nix
    <private>
    <private/ssh.nix>
  ];

  home.username = "alan";
  home.homeDirectory = "/home/alan";
  home.stateVersion = "22.11";

  launchd.agents.colima = {
    enable = true;
    config = {
      ProgramArguments = [ "${pkgs.colima}/bin/colima" "start" ];
      RunAtLoad = true;
      # It doesn't run in the foreground, yet...
      # KeepAlive = true;
      WorkingDirectory = "/Users/alan";
      StandardOutPath = "/Users/alan/Library/Logs/colima.log";
      StandardErrorPath = "/Users/alan/Library/Logs/colima.log";
      EnvironmentVariables.HOME = "/Users/alan";
    };
  };

}
