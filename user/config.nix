{ pkgs }: {
  allowUnfree = true;
  allowUnfreePredicate = pkg: true;
  input-fonts.acceptLicense = true;
}
