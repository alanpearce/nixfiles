{ config
, pkgs
, ...
}: {
  imports = [
    ./settings/base.nix
    ./settings/dunst.nix
    ./settings/emacs.nix
    ./settings/development/base.nix
    ./settings/development/lisp.nix
    ./settings/development/golang.nix
    ./settings/development/javascript.nix
    ./settings/development/web.nix
    ./settings/fish.nix
    ./settings/git.nix
    ./settings/gnupg.nix
    ./settings/i3.nix
    ./settings/music.nix
    ./settings/nixos.nix
    ./settings/rofi.nix
    ./settings/passwords.nix
    ./settings/ssh.nix
    ./settings/sxhkd.nix
    ./settings/tabnine.nix
    ./settings/trezor.nix
    ./settings/user-interface.nix
    ./settings/xresources.nix
    <private>
    <private/ssh.nix>
  ];

  home.username = "alan";
  home.homeDirectory = "/home/alan";
  home.packages = with pkgs; [
    transgui
  ];

  xsession.windowManager.i3.config.startup = [
  ];

  home.stateVersion = "22.11";
}
