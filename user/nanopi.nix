{ pkgs, ... }:
{
  imports = [
    ./settings/base.nix
    ./settings/development/base.nix
    ./settings/fish.nix
    ./settings/git.nix
    ./settings/nix.nix
    ./settings/nixos.nix
    ./settings/music-management.nix
    ./settings/ssh.nix
  ];
  home = {
    username = "alan";
    homeDirectory = "/home/alan";
    stateVersion = "22.11";
  };
  nix.settings = {
    max-jobs = 4;
    experimental-features = "nix-command flakes";
  };
}
