#!/usr/bin/osascript

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Emacs
# @raycast.mode silent

# Optional parameters:
# @raycast.icon https://www.gnu.org/software/emacs/images/emacs.png
# @raycast.packageName Emacs

do shell script "$SHELL -c \"open -a ~/.local/state/nix/profile/Applications/Emacs.app\""
