(setq inhibit-startup-screen t
      initial-scratch-message ""
      initial-major-mode 'fundamental-mode
      frame-inhibit-implied-resize t
      byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local cl-functions))

;; Disable all the bars, unless on macOS, in which case, keep the menu bar.
(unless (eq window-system 'ns)
  (menu-bar-mode -1))
(scroll-bar-mode -1)
(tool-bar-mode -1)
(set-fringe-mode '(4 . 4))

(setenv "LSP_USE_PLISTS" "true") ; must match with lsp-mode override
