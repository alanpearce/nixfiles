{ config
, lib
, pkgs
, ...
}:
with lib; let
  inherit (pkgs) stdenv;
  tomlFormat = pkgs.formats.toml { };
  cfg = config.programs.tabnine;
  lspConfigFile = config:
    pkgs.runCommand "TabNine.toml"
      {
        buildInputs = [ pkgs.remarshal ];
        preferLocalBuild = true;
        allowSubstitutes = false;
      }
      ''
        remarshal -if json -of toml \
        < ${pkgs.writeText "config.json" (builtins.toJSON cfg.lspConfig)} \
        | sed  -e 's/^\["\(.*\)"\]/[\1]/' \
        > $out
      '';
in
{
  options.programs.tabnine = {
    enable = mkEnableOption "TabNine, Smart Compose for code.";

    configDir = mkOption {
      type = types.str;
      default =
        if stdenv.isDarwin
        then "Library/Preferences/TabNine"
        else "${config.xdg.configHome}/TabNine";
      description = ''
        Location of TabNine configuration directory relative to \$HOME
      '';
    };

    config = mkOption {
      type = types.attrs;
      default = { };
      description = ''
        TabNine configuration written to
        <filename>\${configDir}/tabnine_config.json</filename>
      '';
    };

    registrationKey = mkOption {
      type = types.str;
      description = ''
        Registration key for TabNine.  For one-time-purchase users, this should be the e-mail address and base-64-encoded key joined together without spacing.
      '';
    };

    lspConfig = mkOption {
      type = tomlFormat.type;
      default = { };
      description = ''
        LSP Server configuration written to
        <filename>\${configDir}/TabNine.toml</filename>
        </para><para>
      '';
    };
  };

  config = mkIf cfg.enable {
    home.file."${cfg.configDir}/TabNine.toml" = {
      source = (tomlFormat.generate "TabNine.toml" cfg.lspConfig);
    };
    home.file."${cfg.configDir}/tabnine_config.json" = {
      source = pkgs.writeText "tabnine_config.json" (builtins.toJSON cfg.config);
    };
    home.file."${cfg.configDir}/registration_key" = {
      text = cfg.registrationKey;
    };

    programs.neovim.plugins = [
      pkgs.vimPlugins.coc-tabnine
    ];
    programs.neovim.coc.settings = {
      "tabnine.binary_path" = "${pkgs.tabnine}/bin/TabNine";
    };

    programs.emacs.extraPackages = epkgs: [
      epkgs.tabnine
    ];
    programs.emacs.extraConfig = ''
      (defun my/override-tabnine-exe-path (&rest args)
        "${pkgs.tabnine}/bin/TabNine")
      (with-eval-after-load 'tabnine
        (advice-add 'tabnine--executable-path :override #'my/override-tabnine-exe-path))
    '';
  };
}
