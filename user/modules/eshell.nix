{ config
, lib
, pkgs
, ...
}:
with lib; let
  cfg = config.programs.emacs;
in
{
  options.programs.emacs.eshell = {
    aliases = mkOption {
      default = { };
      type = types.attrsOf types.str;
      example = {
        ll = "ls -l $*";
        ff = "find-file $1";
      };
      description = ''
        An attribute set that maps aliases (the top-level attribute names
        in this option) to command strings.
      '';
    };
  };

  config = mkIf cfg.enable {
    xdg.configFile."emacs/eshell/alias" = {
      recursive = true;
      text = concatStringsSep "\n" (mapAttrsToList (k: v: "alias ${k} ${v}") cfg.eshell.aliases);
    };
  };
}
