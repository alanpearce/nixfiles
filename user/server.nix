{ config
, ...
}:
{
  imports = [
    ./settings/base.nix
    ./settings/git.nix
    ./settings/fish.nix
    ./settings/nix.nix
    ./settings/nixos.nix
    ./settings/development/base.nix
    <private>
  ];
  home = {
    username = "alan";
    homeDirectory = "/home/alan";
    stateVersion = "22.11";
  };
}
