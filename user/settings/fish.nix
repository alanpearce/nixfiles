{ pkgs
, lib
, ...
}: {
  programs.fish = {
    enable = true;
    plugins =
      let
        fromNixpkgs = pkg: { name = pkg.name; src = pkg.src; };
      in
      with pkgs.fishPlugins; [
        (fromNixpkgs tide)
        (fromNixpkgs fzf-fish)
        (fromNixpkgs autopair)
        {
          name = "ghq";
          src = pkgs.fetchFromGitHub {
            owner = "decors";
            repo = "fish-ghq";
            sha256 = "0cv7jpvdfdha4hrnjr887jv1pc6vcrxv2ahy7z6x562y7fd77gg9";
            # date = "2021-07-16T13:17:09+09:00";
            rev = "cafaaabe63c124bf0714f89ec715cfe9ece87fa2";
          };
        }
      ];
    # TODO: pre-generate nix-your-shell
    interactiveShellInit = ''
      ${pkgs.nix-your-shell}/bin/nix-your-shell --nom fish env | source
      bind \es __ghq_repository_search
      # don't bind ctrl-t, it does nice things on macOS/BSD
      set FZF_CTRL_T_COMMAND
      set --export FZF_DEFAULT_OPTS '--cycle --layout=reverse --border --height=90% --preview-window=wrap --marker="*"'
      fzf_configure_bindings --directory=\cx\cf
    '';
    shellAliases = {
      hist-freq-lines = lib.mkForce "history | sort | uniq -c | sort -gr | head -n100 | less";
      hist-freq-commands = lib.mkForce "history | cut -d' ' -f 1 | sort | uniq -c | sort -gr | head -n100 | less";
    };
    shellAbbrs = {
      "!!" = {
        position = "anywhere";
        function = "last_history_item";
      };
    };
    functions = {
      ds = "du -hd1 $argv[1] | sort -h";
      last_history_item = "echo $history[1]";
    } // (lib.attrsets.optionalAttrs pkgs.stdenv.isLinux {
      open = ''
        argparse h/help a/application -- $argv
        or return

        if set -ql _flag_help
           echo "open [-h|--help] [-a application] [arguments...]"
           return 1
        end

        if set -ql _flag_application
           # TODO: support reverse-domain- named files (e.g. org.kde.kate.desktop)
           ${pkgs.gtk3}/bin/gtk-launch $argv[1]
        else
          xdg-open $argv
        end
      '';
    });
  };
  xdg.configFile."fish/completions" = {
    recursive = true;
    source = ./fish/completions;
  };
  xdg.configFile."fish/functions" = {
    recursive = true;
    source = ./fish/functions;
  };
}
