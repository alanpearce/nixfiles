{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    cargo
    cargo-edit
    rustc
    rustfmt
    rust-analyzer
    clippy
  ];
}
