{ pkgs, ... }: {
  home.packages = with pkgs; [
    go
    gopls
    godef
    gogetdoc
    gotools
    golines
    impl
    gomodifytags
    golangci-lint
    golangci-lint-langserver
    personal.prettier-plugin-go-template
  ];
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    go-eldoc
    go-tag
  ]);
  programs.neovim.plugins = with pkgs.vimPlugins; [
    coc-go
  ];
}
