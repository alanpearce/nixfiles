{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    clojure
    cljfmt
  ];
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    cider
  ]);
}
