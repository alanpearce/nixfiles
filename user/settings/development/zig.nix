{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    zls
  ];
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    zig-mode
  ]);
  programs.neovim.plugins = with pkgs.vimPlugins; [
    zig-vim
  ];
}
