{ config
, pkgs
, ...
}: {
  home.packages = with pkgs.nodePackages; [
    vscode-css-languageserver-bin
    vscode-html-languageserver-bin
    csslint
    stylelint
  ] ++ (with pkgs; [
    flyctl
    prettierd
    personal.htmlformat
  ]);
  home.shellAliases = {
    # 0.2.25 current completion command only affects `flyctl`, although `fly` is a link to `flyctl`
    fly = "flyctl";
  };
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    caddyfile-mode
    emmet-mode
    nginx-mode
    web-mode
  ]);
  programs.neovim.plugins = with pkgs.vimPlugins; [
    coc-css
    coc-html
  ];
}
