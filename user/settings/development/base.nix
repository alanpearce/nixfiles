{ config
, pkgs
, ...
}:
{
  imports = [ ../../modules/tabnine.nix ];
  home.packages = with pkgs;
    [
      shellcheck
      shfmt
      nodePackages.dockerfile-language-server-nodejs
      nodePackages.vscode-json-languageserver
      nodePackages.yaml-language-server
      lua-language-server
      taplo # toml

      license-cli
      just

      mosh

      xh
      htmlq
      jq
      dasel
      miller
      watchexec
      entr
      httping

      diffoscopeMinimal

      skopeo
      docker-credential-helpers
      dive
    ] ++ (if stdenv.isDarwin then [
      lima-bin
      colima
      docker-client
    ] else [
      zeal
    ]);

  home.sessionVariables = {
    FLY_NO_UPDATE_CHECK = "1";
    LIMA_INSTANCE = "nixos";
  };

  editorconfig = {
    enable = true;
    settings = {
      "*" = {
        charset = "utf-8";
        end_of_line = "lf";
        trim_trailing_whitespace = true;
        insert_final_newline = true;
        indent_style = "space";
        indent_size = 2;
        tab_width = 2;
      };
      "*.fish" = {
        indent_size = 4;
      };
      justfile = {
        indent_style = "tab";
        tab_width = 4;
      };
      Makefile = {
        indent_style = "tab";
        tab_width = 4;
      };
    };
  };

  home.shellAliases = {
    er = "direnv reload";
    ea = "direnv allow";
    ex = "direnv exec";
    es = "direnv status";
  };
  programs.direnv = {
    enable = true;
    nix-direnv = {
      enable = true;
    };
    config = {
      global = {
        disable_stdin = true;
        strict_env = true;
        hide_env_diff = true;
      };
      whitelist = {
        prefix = with config.home; [
          "${homeDirectory}/projects/alanpearce.eu"
        ];
      };
    };
    stdlib = ''
      declare -A direnv_layout_dirs
      direnv_layout_dir() {
        echo "''${direnv_layout_dirs[$PWD]:=$(
          local hash="$(${pkgs.coreutils}/bin/sha256sum - <<<"''${PWD}" | cut -c-7)"
      		local path="''${PWD//[^a-zA-Z0-9]/-}"
          echo "${config.xdg.cacheHome}/direnv/layouts/''${hash}''${path}"
        )}"
      }
    '';
  };
  services.lorri = {
    enable = pkgs.stdenv.isLinux;
    enableNotifications = true;
  };
}
