{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    nim
    nimble
    nimlsp
  ];
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    nim-mode
  ]);
}
