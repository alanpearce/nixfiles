{ config
, pkgs
, ...
}:
{
  home.packages = with pkgs.nodePackages; [
    node2nix
    javascript-typescript-langserver
    typescript-language-server
  ];

  home.sessionVariables = {
    NO_UPDATE_NOTIFIER = "1"; # stop npm update-notifier
  };

  programs.bun = {
    enable = true;
    settings = {
      run = {
        bun = true;
        silent = true;
      };
    };
  };

  programs.emacs.extraPackages = epkgs: (with epkgs; [
    add-node-modules-path
  ]);

  home.shellAliases = {
    bn = "bun";
    bni = "bun install";
    bna = "bun add";
    bnad = "bun add --dev";
    bnr = "bun run";
    bnu = "bun update";
    bnrm = "bun remove";
    bnd = "bun dev";
    bnp = "bun pm";
    bnl = "bun pm ls";
    bnt = "bun test";
    bntw = "bun test --watch";
    bnx = "bunx";

    np = "npm";
    npi = "npm install";
    npl = "npm ls";
    npr = "npm run";
    npb = "npm run build";
    nprb = "npm run build";
    npa = "npm install --save";
    npad = "npm install --save-dev";
    npd = "npm uninstall";
    npt = "npm test";
    npup = "npm update";
    nppr = "npm prune";
    npprp = "npm prune --production";
    npli = "npm link";
    npul = "npm unlink";
  };

  programs.git.attributes = [
    "*.lockb diff=lockb"
  ];
  programs.git.extraConfig."diff.lockb" = {
    textconv = "bun";
    binary = true;
  };

  xdg.configFile.".bunfig.toml".text = ''
    [install.cache]
    dir = "${config.xdg.cacheHome}/bun/"
  '';
  xdg.configFile."npm/config".text = ''
    prefix=''${HOME}/.local
    cache=${config.xdg.cacheHome}/npm/
    always-auth=true
    sign-git-tag=true
    rebuild-bundle=false
    update-notifier=false
    registry=https://registry.npmjs.org/
    @jsr:registry=https://npm.jsr.io
  '';
}
