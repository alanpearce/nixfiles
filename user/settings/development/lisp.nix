{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    clisp
    sbcl
    asdf
    cl-launch
  ] ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux [
    ccl
  ];

  programs.git.attributes = [
    "*.lisp diff=common-lisp"
  ];
  programs.git.extraConfig."diff.common-lisp" = {
    xfuncname = "^\\((def\\S+\\s+\\S+)";
  };
}
