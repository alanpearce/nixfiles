{ config
, pkgs
, ...
}:
let
  inherit (pkgs) stdenv;
in
{
  imports = [
    ./kitty.nix
  ];

  services.ssh-agent = {
    enable = stdenv.hostPlatform.isLinux;
  };

  programs.librewolf = {
    enable = stdenv.hostPlatform.isLinux;
    package = pkgs.librewolf.override {
      nativeMessagingHosts = with pkgs; [
        tridactyl-native
        plasma-browser-integration
        keepassxc
      ];
    };
  };
  home.packages = with pkgs;
    [
      hack-font
      ibm-plex
      inter
      input-fonts
      jetbrains-mono
      meslo-lg
      recursive
      # see https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/data/fonts/nerdfonts/shas.nix
      (nerdfonts.override {
        fonts = [
          "JetBrainsMono"
          "IBMPlexMono"
          "iA-Writer"
          "NerdFontsSymbolsOnly"
        ];
      })
    ]
    ++ lib.optionals (!stdenv.isDarwin) (with pkgs; [
      logseq
      (discord.override { withOpenASAR = true; })

      falkon
      mu
      beeper
    ]);
}
