{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    ledger
    hledger
    bean-add
    beancount
    reckon
  ];
}
