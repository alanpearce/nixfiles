{ config
, pkgs
, ...
}: {
  home.file.".ledgerrc".text = ''
    --date-format %F
    --start-of-week 1
  '';
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    evil-ledger
    ledger-mode
  ]);
}
