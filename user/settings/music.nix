{ pkgs
, ...
}: {
  home.packages = with pkgs; [
    sonixd
  ];

  xdg.desktopEntries.sonixd = {
    name = "sonixd";
    exec = "sonixd";
    comment = "Sonixd Music Player";
    categories = [ "Audio" "AudioVideo" ];
    genericName = "Music Player";
  };
}
