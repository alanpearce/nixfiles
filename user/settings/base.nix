args@{ config
, lib
, pkgs
, ...
}: {
  imports = [
    ./neovim.nix
    ./shell.nix
    (import <nix-index-database/home-manager-module.nix>
      (args // { databases = import <nix-index-database/packages.nix>; }))
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  manual = {
    html.enable = true;
  };
  home.preferXdgDirectories = true;
  home.sessionVariables =
    let
      conf = config.xdg.configHome;
      data = config.xdg.dataHome;
      cache = config.xdg.cacheHome;
      state = config.xdg.stateHome;
    in
    {
      EMAIL = "alan@alanpearce.eu";
      MANPAGER = "bat -l man -p";
      ABDUCO_SOCKET_DIR = "${state}/abduco";
      GNUPGHOME = "${data}/gnupg";
      SOLARGRAPH_CACHE = "${cache}/solargraph";
      ELECTRUMDIR = "${data}/electrum";
      DOCKER_CONFIG = "${conf}/docker";
      npm_config_userconfig = "${conf}/npm/config";
      PASSWORD_STORE_HOME = "${data}/password-store";
      WGETRC = pkgs.writeText "wgetrc" ''
        hsts-file = ${cache}/wget-hsts
      '';
      SQLITE_HISTORY = "${data}/sqlite_history";
    };
  xdg = {
    enable = true;
    cacheHome =
      if pkgs.stdenv.isDarwin
      then "${config.home.homeDirectory}/Library/Caches/XDG"
      else "${config.home.homeDirectory}/.cache";
    configHome = "${config.home.homeDirectory}/.config";
    dataHome = "${config.home.homeDirectory}/.local/share";
    stateHome =
      if pkgs.stdenv.isDarwin
      then "${config.home.homeDirectory}/Library/Caches/TemporaryItems/State"
      else "${config.home.homeDirectory}/.local/state";
  };

  home.shellAliases = {
    make = "remake";
  };
  programs.htop = {
    enable = true;
    settings = {
      header_margin = 0;
      tree_view = 1;
      hide_kernel_threads = 1;
      hide_userland_threads = 1;
    };
  };
  programs.bat = {
    enable = true;
    extraPackages = with pkgs.bat-extras; [
      batdiff
      batman
      batgrep
      batpipe
    ];
    config = {
      theme = "ansi";
      style = "header-filename,header-filesize,rule";
    };
  };
  programs.nix-index-database.comma.enable = true;
  programs.ripgrep = {
    enable = true;
    package = pkgs.ripgrep.override { withPCRE2 = true; };
    arguments = [
      "--smart-case"
    ];
  };
  home.packages = with pkgs;
    [
      pv
      fd
      bfs
      sd
      knot-dns
      dateutils
      moreutils
      mtr
      gping
      tree
      remake
      abduco
      dvtm
      walk
      nuspell
    ] ++ (with pkgs.hunspellDicts; [
      en-gb-large
      de-de
    ]) ++ (
      if !stdenv.isDarwin
      then [
        file
        lsof
        unar
        zip
      ]
      else [
      ]
    );
}
