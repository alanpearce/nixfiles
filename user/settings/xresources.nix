{ config
, pkgs
, ...
}: {
  home.file.".xresources" = {
    recursive = true;
    source = ../xresources;
    onChange = ''
      if [[ -n "$DISPLAY" ]]; then
        ${pkgs.xorg.xrdb}/bin/xrdb -merge .xresources/main
      fi
    '';
  };
}
