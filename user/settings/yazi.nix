{ pkgs
, ...
}: {
  programs.yazi = {
    enable = true;
    enableFishIntegration = true;
    settings = {
      yazi = {
        manager = {
          sort_by = "natural";
          sort_dir_first = true;
        };
      };
    };
  };
}
