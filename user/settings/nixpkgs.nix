{ config
, pkgs
, ...
}:
let
  inherit (pkgs) stdenv;
  stableConfig = config.nixpkgs.config;
in
{
  imports = [
    ./nix.nix
  ];
}
