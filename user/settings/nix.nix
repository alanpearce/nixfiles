{ config
, pkgs
, ...
}:
let
  toml = pkgs.formats.toml { };
in
{
  nixpkgs.config = import ../config.nix;
  nix = {
    enable = true;
    package = pkgs.nix;
    settings = {
      use-xdg-base-directories = true;
    };
  };

  home.packages = with pkgs; [
    nil
    niv
    nix-prefetch-scripts
    nix-init
    nix-update
    nix-tree
    common-updater-scripts
    nixpkgs-fmt
    nixpkgs-lint
    nixpkgs-review
    nix-output-monitor
    cachix
  ];
  xdg.configFile."nix-init/config.toml".source = toml.generate "config.toml" {
    maintainers = [ "alanpearce" ];
    nixpkgs = "<nixpkgs>";
  };
  programs.emacs.extraPackages = epkgs: (with epkgs; [
    nix-mode
    nix-update
  ]);
  programs.neovim.plugins = with pkgs.vimPlugins; [
    vim-nix
  ];
}
