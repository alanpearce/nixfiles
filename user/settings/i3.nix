{ config
, pkgs
, lib
, ...
}: {
  xdg.configFile.i3status = {
    recursive = true;
    source = ../i3/i3status;
  };
  home.sessionVariables = {
    TERMINAL = "xterm";
  };
  xsession.windowManager.i3 =
    let
      mod = "Mod4";
      mode_system = "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown";
      locker = "${pkgs.xautolock}/bin/xautolock -locknow";
    in
    {
      enable = true;
      config = {
        modifier = mod;
        floating = {
          criteria = [
            { class = "Pinentry$"; }
            # https://github.com/ValveSoftware/steam-for-linux/issues/1040
            {
              class = "^Steam$";
              title = "^Friends$";
            }
            {
              class = "^Steam$";
              title = "Steam - News";
            }
            {
              class = "^Steam$";
              title = ".* - Chat";
            }
            {
              class = "^Steam$";
              title = "^Settings$";
            }
            {
              class = "^Steam$";
              title = ".* - event started";
            }
            {
              class = "^Steam$";
              title = ".* CD key";
            }
            {
              class = "^Steam$";
              title = "^Steam - Self Updater$";
            }
            {
              class = "^Steam$";
              title = "^Screenshot Uploader$";
            }
            {
              class = "^Steam$";
              title = "^Steam Guard - Computer Authorization Required$";
            }
            { title = "^Steam Keyboard$"; }
          ];
        };
        window = {
          titlebar = false;
        };
        keybindings = {
          "${mod}+Return" = "exec --no-startup-id ${config.home.sessionVariables.TERMINAL}";
          # kill focused window
          "${mod}+w" = "kill";
          "${mod}+Shift+w" = "kill";

          "${mod}+Mod1+r" = "exec rofi -show run";
          "${mod}+space" = "exec \"rofi -show drun\"";

          # change focus
          "${mod}+h" = "focus left";
          "${mod}+j" = "focus down";
          "${mod}+k" = "focus up";
          "${mod}+l" = "focus right";

          "${mod}+grave" = "workspace 0";
          "${mod}+1" = "workspace 1";
          "${mod}+2" = "workspace 2";
          "${mod}+3" = "workspace 3";
          "${mod}+4" = "workspace 4";
          "${mod}+5" = "workspace 5";
          "${mod}+6" = "workspace 6";
          "${mod}+7" = "workspace 7";
          "${mod}+8" = "workspace 8";
          "${mod}+9" = "workspace 9";
          "${mod}+0" = "workspace 10";

          # move focused window
          "${mod}+Shift+h" = "move left";
          "${mod}+Shift+j" = "move down";
          "${mod}+Shift+k" = "move up";
          "${mod}+Shift+l" = "move right";

          "${mod}+Shift+grave" = "move container to workspace 0";
          "${mod}+Shift+1" = "move container to workspace 1";
          "${mod}+Shift+2" = "move container to workspace 2";
          "${mod}+Shift+3" = "move container to workspace 3";
          "${mod}+Shift+4" = "move container to workspace 4";
          "${mod}+Shift+5" = "move container to workspace 5";
          "${mod}+Shift+6" = "move container to workspace 6";
          "${mod}+Shift+7" = "move container to workspace 7";
          "${mod}+Shift+8" = "move container to workspace 8";
          "${mod}+Shift+9" = "move container to workspace 9";
          "${mod}+Shift+0" = "move container to workspace 10";

          # move workspace
          "${mod}+Mod1+h" = "move workspace to output left";
          "${mod}+Mod1+j" = "move workspace to output down";
          "${mod}+Mod1+k" = "move workspace to output up";
          "${mod}+Mod1+l" = "move workspace to output right";

          # split in horizontal orientation
          "${mod}+b" = "split h";

          # split in vertical orientation
          "${mod}+v" = "split v";

          "${mod}+F11" = "fullscreen toggle";
          "${mod}+f" = "fullscreen toggle";

          # change container layout (stacked, tabbed, toggle split)
          "${mod}+Shift+s" = "layout stacking";
          "${mod}+Shift+t" = "layout tabbed";
          "${mod}+Shift+v" = "layout toggle split";

          # toggle tiling / floating
          "${mod}+Shift+f" = "floating toggle";

          # change focus between tiling / floating windows
          "${mod}+Tab" = "focus mode_toggle";

          # focus the parent container
          "${mod}+a" = "focus parent";

          # focus the child container
          "${mod}+d" = "focus child";

          "${mod}+Shift+c" = "reload";
          "${mod}+Shift+p" = "restart";

          "${mod}+Shift+r" = "mode resize";
          "${mod}+Shift+o" = "exec ${locker}";
          "${mod}+Pause" = "mode \"${mode_system}\"";
          "${mod}+Escape" = "mode \"${mode_system}\"";
          "${mod}+Shift+q" = "mode \"${mode_system}\"";
        };

        modes = {
          resize = {
            "h" = "resize shrink width 10 px or 10 ppt";
            "j" = "resize grow height 10 px or 10 ppt";
            "k" = "resize shrink height 10 px or 10 ppt";
            "l" = "resize grow width 10 px or 10 ppt";

            "Left" = "resize shrink width 10 px or 10 ppt";
            "Down" = "resize grow height 10 px or 10 ppt";
            "Up" = "resize shrink height 10 px or 10 ppt";
            "Right" = "resize grow width 10 px or 10 ppt";

            "Return" = "mode default";
            "Escape" = "mode default";
          };
          "${mode_system}" = {
            "l" = "exec --no-startup-id ${locker}, mode default";
            "e" = "exec --no-startup-id i3-msg exit, mode default";
            "s" = "exec --no-startup-id systemctl suspend, mode default";
            "h" = "exec --no-startup-id systemctl hibernate, mode default";
            "r" = "exec --no-startup-id systemctl reboot, mode default";
            "Shift+s" = "exec --no-startup-id systemctl poweroff -i, mode default";
            "Return" = "mode default";
            "Escape" = "mode default";
          };
        };

        bars = [
          {
            position = "top";
            hiddenState = "show";
            statusCommand = "${pkgs.i3status}/bin/i3status -c ~/.config/i3status/config";
            extraConfig = ''
              font -xos4-terminus-medium-r-normal--12-120-72-72-c-60-iso10646-1
            '';
          }
        ];
      };

      extraConfig = ''
        font -xos4-terminus-medium-r-normal--12-120-72-72-c-60-iso10646-1
      '';
    };
}
