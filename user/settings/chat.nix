{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    signal-desktop
    wire-desktop

    weechat
  ];
}
