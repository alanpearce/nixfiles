{ config
, lib
, pkgs
, ...
}: {
  home.packages = [
    pkgs.html2text
  ];
  accounts.email = {
    maildirBasePath = "mail";
  };
  programs.mbsync = {
    enable = true;
    extraConfig = ''
      CopyArrivalDate yes
      FSync no
    '';
  };
  services.mbsync = {
    enable = true;
    frequency = "*:6/15";
    postExec = "${config.programs.emacs.finalPackage}/bin/emacsclient -e (mu4e-update-index)";
  };
  systemd.user.timers.mbsync = {
    Timer = {
      OnCalendar = lib.mkForce false;
      OnBootSec = "5min";
      OnUnitActiveSec = "15min";
    };
  };
  programs.msmtp = {
    enable = true;
  };
}
