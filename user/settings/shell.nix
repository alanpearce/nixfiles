{ config
, lib
, pkgs
, ...
}:
let
  inherit (pkgs) stdenv;
  lsOptions =
    if stdenv.isDarwin
    then "-p"
    else "-v --group-directories-first --hyperlink=auto";
  lsIsoDate =
    if stdenv.isDarwin
    then ""
    else "--time-style=long-iso";
in
{
  imports = [
    ./yazi.nix
  ];
  home = {
    shellAliases = {
      l = "ls ${lsOptions} -Bp";
      l1 = "ls -1";
      ls = "ls ${lsOptions} -hF";
      la = "ls ${lsOptions} -hA";
      ll = "ls ${lsOptions} ${lsIsoDate} -hl";
      lal = "ll -A";
      lla = "lal";
      llr = "ll -t";

      c = "lk";
      "c," = "cd ${toString ../..}";
      cg = "cd $(git root)";
      cdg = "cd $(git root)";

      cat = "bat --plain";

      fs = "bfs -nohidden";

      http = "xh";
      https = "xh --default-scheme https";
      xhs = "xh --default-scheme https";
      xhh = "xh --print Hh";
      xhhs = "xh --print Hh --default-scheme https";
      xhsh = "xh --print Hh --default-scheme https";

      hist-freq-lines = lib.mkDefault "fc -l -10000 | cut -d' ' -f4- | sort | uniq -c | sort -gr | head -n100 | less";
      hist-freq-commands = lib.mkDefault "fc -l -10000 | cut -d' ' -f4 | sort | uniq -c | sort -gr | head -n100 | less";
      wprop = "xprop | egrep '^WM_(CLASS|NAME|WINDOW_ROLE|TYPE)'";

      # Enable the following commands to support aliases.
      sudo = "sudo ";
      watch = "watch ";

      e = "$EDITOR";
      se = "sudo -e";

      ip = "ip --color=auto";
      ip4 = "ip -4";
      ip6 = "ip -6";

      d = "docker";
      db = "docker build";
      dr = "docker run";
      di = "docker image";
      dl = "docker load";
      dj = "docker pull";
      dk = "docker push";
      dp = "docker ps";
      dps = "docker ps";

      dc = "docker-compose";
      dcb = "dc build";
      dcd = "dc down";
      dcj = "dc pull";
      dck = "dc push";
      dcl = "dc logs";
      dclf = "dc logs -f";
      dcu = "dc up";
      dcud = "dc up -d";
      dcr = "dc restart";

      ga = "git add";
      gs = "git st";
      gd = "git diff";
      gdc = "git diff --cached";
      gf = "git fetch";
      gk = "git push";
      gkf = "git push --force-with-lease";
      gj = "git pull";
      gl = "git lg";
      gls = "git ls";
      gm = "git merge";
      ge = "git remote";
      ges = "git remote -v show";
      gr = "git rebase";
      gz = "git stash";
      gzl = "git stash list";
      gzp = "git stash pop";
      gdt = "git difftool";
      grl = "git reflog";
      gri = "git rebase --interactive";
      gfa = "git fetch --all";
      grs = "git reset";
      grsh = "git reset --hard";
      gsh = "git show";
      gsm = "git submodule";
      gci = "git commit";
      gx = "git restore";
      gb = "git switch";
      gbr = "git br";
      gbrc = "git checkout -b";
      gbrd = "git branch --delete";
      gbrm = "git branch --move";
      gmup = "git mup";
      grup = "git rup";

      ho = "home-manager";
      hob = "home-manager build";
      hos = "home-manager switch -b hm_bak_$(date +%Y%m%d%H%M)";
      hon = "home-manager news";
      hoh = "home-manager help";
      hop = "home-manager packages";
      hol = "home-manager generations";
      hox = "home-manager expire-generations '-30 days'";

      nsh = "nix-shell";
      nb = "nix build";
      nl = "nix log"; # shadows `coreutils.nl`, but I've never used that yet
      nr = "nix run";
      nf = "nix flake";
      nfa = "nix flake archive";
      nfc = "nix flake check";
      nfp = "nix flake prefetch";
      nfu = "nix flake update";
      nfl = "nix flake lock";
      nfsh = "nix shell";
      ndev = "nix develop";
      nlg = "nix-env --list-generations";
      snlg = "sudo nix-env --list-generations --profile /nix/var/nix/profiles/system";
      ngc = "nix-collect-garbage --delete-older-than 30d";
      sngc = "sudo nix-collect-garbage --delete-older-than 30d";
    };
  };
  home.packages = with pkgs; [
    fzf
    up
  ];
}
