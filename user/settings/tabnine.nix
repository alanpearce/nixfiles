{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    tabnine
  ];
  programs.tabnine = {
    enable = pkgs.lib.mkDefault true;
    config = {
      version = pkgs.tabnine.version;

      api_base_url = null;
      api_key = config.programs.tabnine.registrationKey;
      beta_enabled = "No";
      binary_update_interval_seconds = 365 * 24 * 3600;
      cloud_whitelist = [ ];
      creation_time = "2020-12-28T21:42:35.732522096Z";
      deep_completions_work_mode = "LocalOnly";
      disable_auto_update = true;
      disable_local_when_using_battery = false;
      enable_power_saving_mode = false;
      enable_telemetry = false;
      force_local_hub = true;
      generation = 0;
      hide_deep_information_message = false;
      hide_promotional_message = true;
      hosted_deep_completions_enabled = "Disabled";
      ignore_all_lsp = false;
      inline_suggestions_mode = true;
      line_suggestions = true;
      local_enabled = "Yes";
      local_indexing = null;
      local_model_size = null;
      num_of_suggestions = 5;
      onboarding = {
        model_type = null;
        skipped_login = true;
        completed = true;
      };
      rate_limit_amount = null;
      rate_limit_interval_seconds = null;
      semantic_status = {
        css = "Enabled";
        dockerfile = "Enabled";
        go = "Enabled";
        haskell = "Enabled";
        html = "Enabled";
        javascript = "Enabled";
        nix = "Enabled";
        ruby = "Enabled";
        scss = "Enabled";
        typescript = "Enabled";
        yaml = "Enabled";
      };
      snippets_enabled_v2 = true;
      tabnine_cloud_certificate_domain = null;
      tabnine_cloud_host = null;
      tabnine_cloud_port = null;
      use_specialized_model_if_available = true;
    };
    lspConfig.language = {
      typescript = {
        command = "typescript-language-server";
        args = [ "--stdio" ];
      };
      javascript = {
        command = "javascript-typescript-stdio";
        args = [ "--stdio" ];
      };
      css = {
        command = "css-languageserver";
        args = [ "--stdio" ];
      };
      scss = {
        command = "css-languageserver";
        args = [ "--stdio" ];
      };
      html = {
        command = "html-languageserver";
        args = [ "--stdio" ];
      };
      nix = {
        command = "nil";
        args = [ "--stdio" ];
      };
      dockerfile = {
        command = "docker-langserver";
        args = [ "--stdio" ];
      };
      ruby = {
        command = "solargraph";
        args = [ "stdio" ];
      };
      yaml = {
        command = "yaml-language-server";
        args = [ "--stdio" ];
      };
      haskell = {
        command = "hie";
        args = [ "--stdio" ];
      };
      go = {
        command = "gopls";
        args = [
          "serve"
        ];
      };
    };
  };
}
