{ config
, lib
, pkgs
, ...
}: {
  programs.ssh = {
    enable = true;
    compression = true;
    controlMaster = "auto";
    controlPersist = "10m";
    hashKnownHosts = true;
    serverAliveInterval = 15;
    extraConfig = ''
      VerifyHostKeyDNS ask
    '';
    includes = [
      "local.ssh_config"
    ];
  };
}
