{ pkgs
, ...
}:
{
  programs.kitty = {
    enable = true;
    font = {
      package = pkgs.recursive;
      name = "Rec Mono SemiCasual";
      size = 15;
    };
    shellIntegration = {
      mode = "no-cursor";
    };
    keybindings = {
      "ctrl+shift+t" = "new_tab_with_cwd !neighbor";
      "cmd+t" = "new_tab_with_cwd !neighbor";
    };
    settings = {
      macos_option_as_alt = "left";
    };
    extraConfig = ''
      include ~/.config/kitty/theme.conf
    '';
  };
}
