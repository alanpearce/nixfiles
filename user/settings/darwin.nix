{ config
, pkgs
, lib
, ...
}: {
  home.packages = with pkgs; [
    darwin.trash
    nh
    maid
    less
  ];

  programs.emacs.package = pkgs.emacs29.overrideAttrs (old: {
    NIX_CFLAGS_COMPILE = (old.NIX_CFLAGS_COMPILE or "") + " -DFD_SETSIZE=10000 -DDARWIN_UNLIMITED_SELECT";
  });

  home.file.".hushlogin".text = "";

  services.syncthing = {
    enable = true;
  };

  launchd.agents = {
    home-manager-expire-generations = {
      enable = true;

      config = {
        ProgramArguments = [
          "${pkgs.home-manager}/bin/home-manager"
          "expire-generations"
          "-30 days"
        ];
        KeepAlive = false;
        RunAtLoad = false;
        StartCalendarInterval = [{
          Hour = 12;
          Minute = 00;
          Weekday = 6; # Saturday
        }];
        ProcessType = "Background";
        LowPriorityBackgroundIO = true;
      };
    };
    setenv = {
      enable = true;

      config = {
        ProgramArguments = [
          "/bin/launchctl"
          "setenv"
          "XDG_CACHE_HOME"
          config.xdg.cacheHome
        ];
        RunAtLoad = true;
        StandardErrorPath = "/dev/null";
        StandardOutputPath = "/dev/null";
      };
    };
    dark-light-mode = {
      enable = true;
      config = {
        WatchPaths = [ "${config.home.homeDirectory}/Library/Preferences/.GlobalPreferences.plist" ];
        StandardOutputPath = "/dev/null";
        StandardErrorPath = "/dev/null";
        RunAtLoad = true;
        KeepAlive = false;
        ProgramArguments = [
          "/bin/sh"
          (
            toString
              (
                pkgs.writeShellScript
                  "toggle-dark-light-mode"
                  ''
                    wait4path /nix
                    if defaults read -g AppleInterfaceStyle &>/dev/null ; then
                      MODE="dark"
                    else
                      MODE="light"
                    fi
                    emacsclient="${config.programs.emacs.finalPackage}/bin/emacsclient"
                    kitty="${pkgs.kitty}/bin/kitty +kitten themes --config-file-name=theme.conf --reload-in=all --cache-age=-1"
                    emacsSwitchTheme () {
                      if pgrep -q Emacs; then
                        if [[  $MODE == "dark"  ]]; then
                            $emacsclient \
                              --eval "(stimmung-themes-load-dark)" \
                              --eval "(modify-all-frames-parameters '((ns-appearance '$MODE)))"
                        elif [[  $MODE == "light"  ]]; then
                            $emacsclient \
                              --eval "(stimmung-themes-load-light)" \
                              --eval "(modify-all-frames-parameters '((ns-appearance '$MODE)))"
                        fi
                      fi
                    }
                    kittySwitchTheme () {
                      if pgrep -q kitty; then
                        if [[  $MODE == "dark"  ]]; then
                          $kitty 'Modus Vivendi'
                        elif [[ $MODE == "light" ]]; then
                          $kitty 'Modus Operandi'
                        fi
                      fi
                    }
                    emacsSwitchTheme
                    kittySwitchTheme
                  ''
              )
          )
        ];
      };
    };
  };

  home.shellAliases = {
    rb = "darwin-rebuild";
    rbs = "darwin-rebuild switch";

    dig = "dig +noall +answer";

    stat = "stat -x";
  };

  programs.ssh.extraConfig = ''
    IdentityAgent "~/.strongbox/agent.sock"
  '';

  home.activation.linkStrongboxSSHAgentSocket = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
    if [[ ! -d ~/.strongbox ]]
    then
      $DRY_RUN_CMD mkdir ~/.strongbox
    fi
    if [[ ! -S ~/.strongbox/agent.sock ]]
    then
      $DRY_RUN_CMD ln -s $VERBOSE_ARG \
        ~/Library/Group\ Containers/group.strongbox.mac.mcguill/agent.sock ~/.strongbox/agent.sock
    fi
  '';

  # Use GPG from  GPGTools
  programs.git.signing.gpgPath = "/usr/local/bin/gpg";
}
