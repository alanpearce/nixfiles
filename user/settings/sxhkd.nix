{ config
, pkgs
, ...
}:
let
  pamixer = "${pkgs.pamixer}/bin/pamixer";
  light = "${pkgs.light}/bin/light";
  playerctl = "${pkgs.playerctl}/bin/playerctl";
  networkmanager_dmenu = "${pkgs.networkmanager_dmenu}/bin/networkmanager_dmenu";
in
{
  xdg.configFile."sxhkd/sxhkdrc".text = ''
    XF86AudioMute
      ${pamixer} --toggle-mute

    XF86Audio{Lower,Raise}Volume
      ${pamixer} --{decrease,increase} 1

    @XF86AudioMicMute
      ${pamixer} --source 2 --toggle-mute

    XF86MonBrightness{Down,Up}
      ${light} -{U,A} 2%

    XF86AudioPlay
      ${playerctl} play-pause

    XF86Audio{Prev,Next}
      ${playerctl} {previous,next}

    XF86Tools
      ${networkmanager_dmenu}
  '';
}
