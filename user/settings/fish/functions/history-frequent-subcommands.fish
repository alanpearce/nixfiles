function history-frequent-subcommands
    history search --prefix "$argv[1] " --max 10000 | cut -d' ' -f 2 | sort | uniq -c | sort -gr | head -n100 | less
end
