function dired
    if set --query argv[1]
        set --function repo $argv[1]
    else
        set --function repo $pwd
    end
    emacsclient -e "(dired \"$argv[1]\")"
end
