function delink
    if test ! -L $argv[1]
        echo "$argv[1] is not a symlink"
        return 1
    end
    set -l src (readlink $argv[1])
    rm $argv[1]
    cp $src $argv[1]
    chmod u+w $argv[1]
end
