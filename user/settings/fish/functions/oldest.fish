function oldest
    if test (count $argv) -eq 0
        echo "Need at least one path"
        return 1
    end
    for arg in $argv
        if test -d $arg
            fd --hidden --print0 --max-depth 1 . $arg |
                bfs -files0-from - \( -name .git -prune \) -o \( -printf '%TY%Tm%Td%TH%TM %TF %h/%f\n' \) |
                sort --key 1n,1 | head --lines 1 |
                cut -d ' ' -f 2,3
        end
    end
end
