{ config
, pkgs
, lib
, ...
}:
let
  inherit (pkgs) stdenv;

  nativeCompileDirectory = "${config.xdg.cacheHome}/emacs/native-compile/";

  editorScript = pkgs.writeScriptBin "edit" ''
    #!${pkgs.runtimeShell}
    if [ -z "$1" ]; then
      exec ${config.programs.emacs.finalPackage}/bin/emacsclient --create-frame --alternate-editor ${config.programs.emacs.finalPackage}/bin/emacs
    else
      exec ${config.programs.emacs.finalPackage}/bin/emacsclient --alternate-editor ${config.programs.emacs.finalPackage}/bin/emacs --create-frame "$@"
    fi
  '';
in
{
  imports = [
    ../modules/eshell.nix
  ];

  programs.git.attributes = [
    "*.el diff=elisp"
  ];
  programs.git.extraConfig."diff.elisp" = {
    xfuncname = "^(((;;;+ )|\\(|([ \t]+\\(((cl-|el-patch-)?def(un|var|macro|method|custom)|gb/))).*)$";
  };

  services.emacs = lib.mkIf stdenv.isLinux {
    enable = true;
    package = config.programs.emacs.finalPackage;
    client.enable = true;
  };
  programs.emacs = {
    enable = true;
    package = lib.mkDefault (pkgs.emacs29.override { withGTK3 = true; });

    eshell = {
      aliases = {
        pk = "eshell-up-pk $1";
        up = "eshell-up $1";

        ec = "find-file $1";

        l = "ls $*";
        la = "ls -A $*";
        ll = "ls -lh $*";
        lla = "ls -lhA $*";

        http = "xh";
        https = "xh --default-scheme https $*";
        xhs = "xh --default-scheme https $*";

        ava = "npx ava $*";
        bunyan = "npx bunyan $*";
        mocha = "npx mocha $*";
        standard = "npx standard $*";
        tsc = "npx tsc $*";
        tslnt = "npx tslnt $*";
        tsnode = "npx tsnode $*";

        cdg = "cd (project-root)";
      };
    };

    extraPackages = epkgs: (with epkgs;
      [
        ace-link
        apheleia
        astro-ts-mode
        avy
        benchmark-init
        cape
        clojure-mode
        cask-mode
        corfu
        consult
        consult-dir
        consult-ghq
        consult-eglot
        consult-lsp
        crux
        dired-git-info
        docker-compose-mode
        dtrt-indent
        envrc
        editorconfig
        eldoc-box
        embark
        embark-consult
        esh-buf-stack
        esh-help
        eshell-fringe-status
        eshell-toggle
        eshell-up
        evil
        evil-anzu
        evil-collection
        evil-commentary
        evil-embrace
        evil-exchange
        evil-lion
        evil-matchit
        evil-mu4e
        evil-numbers
        evil-org
        evil-quickscope
        evil-space
        evil-surround
        evil-textobj-tree-sitter
        expand-region
        eyebrowse
        fish-mode
        feature-mode
        format-all
        flycheck
        flymake-popon
        general
        git-gutter-fringe
        git-modes
        git-timemachine
        gl-conf-mode # gitolite
        goto-chg
        helpful
        jinx
        just-mode
        justl
        kind-icon
        lua-mode
        lsp-mode
        lispyville
        magit
        magit-todos
        markdown-mode
        marginalia
        nerd-icons
        nix-ts-mode
        orderless
        doom-modeline
        php-mode
        persist-state
        posframe
        quickrun
        rainbow-mode
        rainbow-delimiters
        stimmung-themes
        systemd
        tempel
        tempel-collection
        eglot-tempel
        treemacs
        treemacs-evil
        treemacs-magit
        treemacs-nerd-icons
        treesit-grammars.with-all-grammars
        treesit-auto
        vc-msg
        vertico
        vertico-prescient
        wgrep-ag
        ws-butler
        which-key
        yasnippet
        yasnippet-capf
      ]);
    overrides = self: super: {
      apheleia = self.melpaPackages.apheleia.overrideAttrs
        (old: {
          patchPhase = ''
            substituteInPlace apheleia-formatters.el \
              --replace-fail '"prettier"' '"prettierd"'
          '';
        });
      just-mode = self.melpaPackages.just-mode.overrideAttrs (old: {
        src = pkgs.fetchFromGitHub {
          owner = "alanpearce";
          repo = "just-mode.el";
          rev = "08eb25e0641b4b6d79aa39182c70b9d40c56fc02";
          sha256 = "13ccphbd95bn79pqbw6ycnfy1z8yd32swrhd1ljl7gwbhi7q6s0p";
          # date = "2024-05-01T22:22:02+02:00";
        };
      });
      treemacs-nerd-icons = self.melpaPackages.treemacs-nerd-icons.overrideAttrs (old: {
        src = pkgs.fetchFromGitHub {
          owner = "aaronmiller";
          repo = "treemacs-nerd-icons";
          sha256 = "171pdi5y9zym26iqi02c5p7zw9i7xxhv4csnjb7qlkkczha17jgp";
          rev = "90b4f0868eea1ea923dee97d2c5457c21a61f37a";
          # date = "2023-11-02T13:42:55-04:00";
        };
      });
      lsp-mode = self.melpaPackages.lsp-mode.overrideAttrs {
        LSP_USE_PLISTS = "true"; # must be set in early-init
      };
    };
    extraConfig = ''
      (with-eval-after-load 'editorconfig
        (defvar editorconfig-exec-path "${pkgs.editorconfig-core-c}/bin/editorconfig"))
      (when (featurep 'native-compile)
        (defvar native-compile-target-directory "${nativeCompileDirectory}")
        (add-to-list 'native-comp-eln-load-path "${nativeCompileDirectory}" :append))
    '' + lib.optionalString stdenv.isDarwin ''
      (with-eval-after-load 'files
        (defvar insert-directory-program "${pkgs.coreutils-prefixed}/bin/gls"))
      (with-eval-after-load 'dired
        (defvar dired-use-ls-dired t))
    '';
  };
  home.packages = with pkgs; [
    editorScript
    enchant
  ];
  xdg.configFile."raycast/scripts/Emacs" = {
    executable = true;
    source = ../emacs/raycast-script.applescript;
  };
  xdg.configFile."emacs/early-init.el" = {
    source = ../emacs/early-init.el;
  };
  xdg.configFile."emacs/init.el" = {
    source = ../emacs/init.el;
  };
}
