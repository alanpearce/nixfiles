{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    (beets.override {
      pluginOverrides = {
        alternatives = {
          enable = true;
          propagatedBuildInputs = [
            beetsPackages.alternatives
          ];
        };
      };
    })
  ];
}
