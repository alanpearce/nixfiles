{ config
, pkgs
, ...
}: {
  programs.git = {
    enable = true;
    userName = "Alan Pearce";
    userEmail = "alan@alanpearce.eu";
    delta = {
      enable = true;
      options = {
        navigate = true;
        light = true;
      };
    };
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      ghq = {
        root = "${config.home.homeDirectory}/projects";
        user = "alanpearce";
      };
      core = {
        commitGraph = true;
      };
      pull = {
        rebase = true;
      };
      push = {
        default = "current";
        followTags = true;
      };
      rebase = {
        autosquash = true;
      };
      rerere = {
        enable = true;
      };
      merge = {
        conflictStyle = "diff3";
      };
      diff = {
        algorithm = "patience";
        colorMoved = "default";
      };
      remote = {
        autoSetupMerge = true;
      };
      "branch.master" = {
        rebase = false;
      };
      "branch.main" = {
        rebase = false;
      };
    };
    signing = {
      key = "0xCD4BEB92A8D46583";
    };
    lfs = {
      enable = true;
    };
    aliases = {
      authors = "shortlog -s -n";
      mup = "merge FETCH_HEAD";
      rup = "rebase FETCH_HEAD";
      ls = "ls-files";
      st = "status -sb";
      ci = "commit";
      br = "branch";
      co = "checkout";
      sw = "switch";
      rs = "restore";
      lasttag = "!sh -c 'git tag --sort=version:refname | grep \"^v\\\\?[0-9]\" | tail -n1'";
      pending = "!sh -c 'git log --oneline --grep=\"#\" ...$(git lasttag)'";
      lg = "log --pretty=format:'%Cred%h%Creset -%Creset %s %Cgreen(%cr) %C(bold blue)<%an> %Cred%d%Creset'";
      prl = "log --pretty=format:'%Cred%h%Creset -%Creset %s %Cgreen(%cr) %C(bold blue)<%an> %Cred%d%Creset'  --grep='#'";
      gen-ignore = "ignore-io";
      ignored = "ls-files --others -i --exclude-standard";
      clear = "clear-soft";
      clear-hard = "!git-clear-hard";
    };
    ignores = [
      ".DS_Store"
      "*_flymake.*"
      "*~"
      "\#*\#"
      ".\#*"
      ".tabnine_root"
    ];
  };
  programs.gh = {
    enable = true;
    settings = {
      git_protocol = "ssh";
      aliases = {
        fork = "repo fork --remote --remote-name alanpearce --default-branch-only";
      };
    };
  };
  home.packages = with pkgs; [
    git-extras # delete-merged-branches and friends
    ghq
    delta
    gitui
    gitstatus
    hut # sourcehut tools
  ];
}
