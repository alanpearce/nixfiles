{ config
, pkgs
, ...
}: {
  home.packages = with pkgs; [
    keepassxc
    pwgen

    python3Packages.keyring
  ];
}
