{ config
, pkgs
, ...
}: {
  services.dunst = {
    enable = true;
    settings = {
      global = {
        font = "Monospace 13";
        geometry = "300x10-10+30";
        follow = "none";
        sticky_history = "yes";
        history_length = 20;
        show_indicators = "yes";
        separator_height = 2;
        padding = 8;
        horizontal_padding = 8;
        idle_threshold = 30;

        dmenu = "${pkgs.rofi}/bin/dmenu -p dunst";

        indicate_hidden = "no";

        allow_markup = "yes";
        format = "<b>%s</b>\\n%b";
        word_wrap = "yes";
        ignore_newline = "no";

        separator_color = "frame";

        mouse_left_click = "do_action";
        mouse_right_click = "close_current";
      };
      frame = {
        width = 1;
        color = "#383838";
      };
      shortcuts = {
        close = "mod4+apostrophe";
        close_all = "mod4+shift+apostrophe";
        history = "mod4+grave";
        context = "mod4+shift+period";
      };
      urgency_low = {
        background = "#b8b8b8";
        foreground = "#f8f8f8";
        timeout = 10;
      };

      urgency_normal = {
        background = "#7cafc2";
        foreground = "#f8f8f8";
        timeout = 10;
      };

      urgency_critical = {
        background = "#ab4642";
        foreground = "#f8f8f8";
        timeout = 0;
      };
    };
  };
}
