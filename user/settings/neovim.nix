{ pkgs
, ...
}: {
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    withNodeJs = true;
    plugins = with pkgs.vimPlugins; [
      commentary
      conjure
      coc-git
      coc-json
      coc-yaml
      direnv-vim
      (nvim-treesitter.withPlugins (
        plugins: with plugins; [
          nix
        ]
      ))
      telescope-nvim
      telescope-fzf-native-nvim
      vim-polyglot
      vim-tridactyl
      vim-surround
    ];
    extraLuaConfig = builtins.readFile ../nvim/init.lua;
    coc = {
      enable = true;
      settings = {
        "json.enable" = true;
        "suggest.enablePreview" = true;
        "coc.preferences.formatOnSaveFiletypes" = [ "nix" ];
        "nil.formatting.command" = "nixpkgs-fmt";
        semantictokens = {
          filetypes = [ "nix" ];
        };
        languageserver = {
          nix = {
            command = "${pkgs.nil}/bin/nil";
            filetypes = [ "nix" ];
          };
          lua = {
            command = "${pkgs.lua-language-server}/bin/lua-language-server";
            filetypes = [ "lua" ];
          };
        };
      };
    };
  };
}
