{ config
, lib
, pkgs
, ...
}: {
  xdg.configFile."networkmanager-dmenu/config.ini".text = ''
    [dmenu]
    dmenu_command = ${pkgs.rofi}/bin/dmenu

    [editor]
    terminal = ${config.home.sessionVariables.TERMINAL}
    gui_if_available = false
  '';

  programs.rofi = {
    enable = true;
    theme = "Arc";
    extraConfig = {
      matching = "glob";
      separator-style = "none";
      modi = "drun,run";
      line-padding = "2";
      display-run = "cmd";
      display-drun = "run";
      display-window = "win";
    };
  };
}
