{ ... }: {
  imports = [
    ./nix.nix
    ./nixpkgs.nix
  ];

  home.shellAliases = {
    srb = "nixos-rebuild";
    rbs = "nixos-rebuild switch";
    rbb = "nixos-rebuild boot";
    rbr = "nixos-rebuild switch --rollback";
  };
}
