let
  sources = import ./nix/sources.nix;

  nixpkgs = sources.nixpkgs;
  pkgs = import nixpkgs { };
  lib = import ./lib { inherit pkgs; };

  nixPath = builtins.concatStringsSep ":" (lib.mkNixPath sources);
in
pkgs.mkShell
{

  name = "nixfiles-shell";

  buildInputs = with pkgs; [
    niv
    deploy-rs
    (pkgs.callPackage "${sources.agenix}/pkgs/agenix.nix" { })
    (import sources.home-manager { inherit pkgs; }).home-manager
  ];

  shellHook = ''
    export NIX_PATH="${nixPath}";
  '';

}
